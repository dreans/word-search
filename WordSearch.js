var file = document.getElementById("inputfile");

file.addEventListener("change", () => {
    var txtArr = [];
    var mark = [];
    var wordSearch;
    var arrText;
    var newArr;
    var fr = new FileReader();
    fr.onload = function () {
        // By lines
        var data = this.result.toUpperCase().split("\r\n");
        txtArr = data;
    };

    // function in_path(path, row, col) {
    //     for (let i = 0; i < newArr.length; i++) {
    //         if (path[i].row == row && path[i].col == col) {
    //             return true;
    //         }
    //     }
    //     return false;
    // }

    // function search(row, col, word, path) {
    //     // mark[0][0] = true;
    //     if (word == "") {
    //         path.forEach(function (e) {
    //             mark[e.row][e.col] = true;
    //         });
    //     } else if (
    //         0 <= row &&
    //         row < newArr.length &&
    //         0 <= col &&
    //         col < newArr[row].length &&
    //         !in_path(path, row, col) &&
    //         word[0] == newArr[row][col]
    //     ) {
    //         for (let r = -1; r <= 1; r++) {
    //             for (let c = -1; c <= 1; c++) {
    //                 search(
    //                     row + r,
    //                     col + r,
    //                     word.substr(1),
    //                     path.concat({ row: row, col: col })
    //                 );
    //             }
    //         }
    //     }
    // }

    fr.onloadend = function () {
        // console.log(txtArr);
        let skipValue = Number(txtArr[0]);
        console.log(skipValue);
        let numberWordSearch = Number(txtArr[1]);
        // console.log(numberWordSearch);
        wordSearch = txtArr.slice(2, numberWordSearch + 2);
        console.log(wordSearch);
        let lines = Number(txtArr[numberWordSearch + 2]);
        console.log(lines);
        let strText = txtArr
            .slice(numberWordSearch + 3, lines + numberWordSearch + 3)
            .join("");
        // console.log(strText);
        arrText = strText.replace(/[-.,;: ']/g, "");
        // console.log(arrText.length);
        // console.log(arrText);

        function* chunks(arr, n) {
            for (let i = 0; i < arr.length; i += n) {
                yield arr.slice(i, i + n);
            }
        }
        newArr = [...chunks(arrText, skipValue)];
        console.log(newArr);

        for (let i = 0; i < newArr.length; i++) {
            mark[i] = [];
        }

        // wordSearch.forEach(function (word) {
        //     for (let row = 0; row < skipValue; row++) {
        //         for (let col = 0; col < newArr[row].length; col++) {
        //             search(row, col, word, []);
        //         }
        //     }
        // });

        document.write("<table border='1'>");
        for (let row = 0; row < skipValue; row++) {
            document.write("<tr>");
            for (let col = 0; col < newArr.length; col++) {
                var color = mark[row][col] ? "orange" : "white";
                document.write(
                    "<td align='center' bgcolor='" +
                        color +
                        "'>" +
                        newArr[col][row]
                ) + "</td>";
            }
            document.write("<tr>");
        }
    };
    fr.readAsText(file.files[0]);
});
